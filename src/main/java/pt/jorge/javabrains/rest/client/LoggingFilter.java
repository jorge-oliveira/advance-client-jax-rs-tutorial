package pt.jorge.javabrains.rest.client;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
public class LoggingFilter implements ContainerResponseFilter {
    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        System.out.println("Request Filter:");
        System.out.println("Headers: " + containerRequestContext.getHeaders());

        System.out.println("Response Filter: ");
        System.out.println("Headers: " + containerResponseContext.getHeaders());
    }

}
