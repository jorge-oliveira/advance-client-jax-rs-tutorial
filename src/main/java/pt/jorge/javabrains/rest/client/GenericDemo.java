package pt.jorge.javabrains.rest.client;

import pt.jorge.javabrains.messenger.model.Message;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class GenericDemo {
    public static void main(String[] args) {

        Client client = ClientBuilder.newClient();

        // common target
        List<Message> messages = client.target("http://localhost:8083/myApp/webapi/")
                // message target
                .path("messages")
                .queryParam("year", 2018)
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Message>>() {
                }); // change the return from a response to a list

        System.out.println(messages);
    }
}
