package pt.jorge.javabrains.rest.client;

import pt.jorge.javabrains.messenger.model.Message;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class RestApiClient {

    public static void main(String[] args) {

        Client client = ClientBuilder.newClient();

        // common target
        WebTarget baseTarget = client.target("http://localhost:8083/myApp/webapi/");
        // message target
        WebTarget messagesTarget = baseTarget.path("messages");
        // set the generic message id
        WebTarget singleMessageTarget = messagesTarget.path("{messageId}");

/*        // set the target
        Message message = singleMessageTarget
                .resolveTemplate("messageId", "1")
                // set the type of media type
                .request(MediaType.APPLICATION_JSON)
                // create an http request (GET)
                .get(Message.class);

        // set the target
        Message message2 = singleMessageTarget
                .resolveTemplate("messageId", "2")
                .request(MediaType.APPLICATION_JSON)
                .get(Message.class);

        // get the message response
        System.out.println(message.getMessage());
        System.out.println(message2.getMessage());
        */

        // implementing a post message
        Message newMessage = new Message(4, "My new message from jax-rs client", "Oliveira");
        Response postResponse = messagesTarget
                .request()
                .post(Entity.json(newMessage)); // convert into json

        if (postResponse.getStatus() != 201) {
            System.out.println("Error....");
        }
        Message createMessage = postResponse.readEntity(Message.class);
        System.out.println(createMessage.getMessage());

    }
}
