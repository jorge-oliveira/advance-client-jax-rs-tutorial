package pt.jorge.javabrains.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("test") // http://localhost:8083/myApp/webapi/test
public class MyResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String testMethod() {
        return "It Works Fine...";
    }
}
