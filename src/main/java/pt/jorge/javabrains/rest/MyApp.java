package pt.jorge.javabrains.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("webapi") // set the first start point path
public class MyApp extends Application {

}
