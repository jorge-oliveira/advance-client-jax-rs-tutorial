package pt.jorge.javabrains.messenger.resources;//package org.koushik.javabrains.messenger.resources;
////
////import java.io.File;
////import java.io.FileOutputStream;
////import java.io.IOException;
////import java.io.InputStream;
////import java.io.OutputStream;
////import javax.ws.rs.Consumes;
////import javax.ws.rs.POST;
////import javax.ws.rs.Path;
////import javax.ws.rs.core.MediaType;
////import javax.ws.rs.core.Response;
////import com.sun.jersey.core.header.FormDataContentDisposition;
////import com.sun.jersey.multipart.FormDataParam;
////
////@Path("/file")
////public class UploadFileService {
////
////    @POST
////    @Path("/upload")
////    @Consumes(MediaType.MULTIPART_FORM_DATA)
////    public Response uploadFile(
////            @FormDataParam("file") InputStream uploadedInputStream,
////            @FormDataParam("file") FormDataContentDisposition fileDetail) {
////
////        String uploadedFileLocation = "d://uploaded/" + fileDetail.getFileName();
////
////        // save it
////        writeToFile(uploadedInputStream, uploadedFileLocation);
////
////        String output = "File uploaded to : " + uploadedFileLocation;
////
////        return Response.status(200).entity(output).build();
////
////    }
////
////    // save uploaded file to new location
////    private void writeToFile(InputStream uploadedInputStream,
////                             String uploadedFileLocation) {
////
////        try {
////            int read = 0;
////            byte[] bytes = new byte[1024];
////
////            OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
////            while ((read = uploadedInputStream.read(bytes)) != -1) {
////                out.write(bytes, 0, read);
////            }
////            out.flush();
////            out.close();
////        } catch (IOException e) {
////
////            e.printStackTrace();
////        }
////
////    }
////
////}
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//
//import javax.ws.rs.Consumes;
//import javax.ws.rs.POST;
//import javax.ws.rs.Path;
//import javax.ws.rs.WebApplicationException;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//
//import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
//import org.glassfish.jersey.media.multipart.FormDataParam;
//
//@Path("/file")
//public class UploadFileService {
//
//    @POST
//    @Path("/upload")
//    @Consumes({MediaType.MULTIPART_FORM_DATA})
//    public String uploadPdfFile(@FormDataParam("file") InputStream fileInputStream,
//                                @FormDataParam("file") FormDataContentDisposition fileMetaData) throws Exception
//    {
//        String UPLOAD_PATH = "/tmp/upload/";
////        String UPLOAD_PATH = "d://uploaded/";
//        try
//        {
//            int read = 0;
//            byte[] bytes = new byte[1024];
//
//            OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + fileMetaData.getFileName()));
//            while ((read = fileInputStream.read(bytes)) != -1)
//            {
//                out.write(bytes, 0, read);
//            }
//            out.flush();
//            out.close();
//        } catch (IOException e)
//        {
//            throw new WebApplicationException("Error while uploading file. Please try again !!");
//        }
////        return Response.ok("Data uploaded successfully !!").build();
//        return "File created.";
//    }
//}
