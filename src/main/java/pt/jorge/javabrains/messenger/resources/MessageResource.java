package pt.jorge.javabrains.messenger.resources;

import pt.jorge.javabrains.messenger.model.Message;
import pt.jorge.javabrains.messenger.service.MessageService;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Path("/messages") // http://localhost:8080/webapi/messages
// added in top for all the methods down have inheritance
// @Produces(MediaType.APPLICATION_XML) // convert the response to Xml
@Consumes(MediaType.APPLICATION_JSON) // accepts the context as a json
// adds support for all methods in json and xml
//@Produces(value = {MediaType.APPLICATION_JSON,
//        MediaType.TEXT_XML})
@Produces(MediaType.APPLICATION_JSON)
public class MessageResource {

    MessageService messageService = new MessageService();

//    // default path
//    @GET
//    // @Produces(MediaType.APPLICATION_XML) // convert the response to Xml
////    @Produces(MediaType.APPLICATION_JSON) // convert the response to jason
//    public List<Message> getMessages() {
//        return messageService.getAllMessages();
//    }


    // http://localhost:8088/webapi/messages
    // http://localhost:8088/webapi/messages?year=2018
    //http://localhost:8088/webapi/messages?start=1&size=1

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Message> getJsonMessages(@QueryParam("year") int year,
                                     @QueryParam("start") int start,
                                     @QueryParam("size") int size) {
        if (year > 0) {
            return messageService.getAllMessagesForYear(year);
        }
        if (start >= 0 && size > 0) {
            return messageService.getAllMessagesPaginated(start, size);
        }
        return messageService.getAllMessages();
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public List<Message> getXmlMessages(@QueryParam("year") int year,
                                     @QueryParam("start") int start,
                                     @QueryParam("size") int size) {
        if (year > 0) {
            return messageService.getAllMessagesForYear(year);
        }
        if (start >= 0 && size > 0) {
            return messageService.getAllMessagesPaginated(start, size);
        }
        return messageService.getAllMessages();
    }

//
//    @POST
//    public Message addMessage(Message message) {
//        return messageService.addMessage(message);
//    }

    // add a custom response
    @POST
    public Response addMessage(Message message, @Context UriInfo uriInfo) throws URISyntaxException {
        Message newMessage = messageService.addMessage(message);
        String newId = String.valueOf(newMessage.getId());
        // get a url of the class path
        URI uri = uriInfo.getAbsolutePathBuilder().path(newId).build();
        return Response.created(uri)
                .entity(newMessage)
                .build();
    }

    @PUT
    @Path("/{messageId}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
    public Message updateMessage(@PathParam("messageId") long messageId, Message message) {
        message.setId(messageId);
        return messageService.updateMessage(message);
    }

    @DELETE
    @Path("/{messageId}")
    public void deleteMessage(@PathParam("messageId") long messageId) {
        messageService.removeMessage(messageId);
    }

    @GET
    @Path("/{messageId}")   // mapping the path with a variable id
//    @Produces(MediaType.APPLICATION_JSON) // convert the response to jason
    public Message getMessage(@PathParam("messageId") long messageId, @Context UriInfo uriInfo) {
//        return messageService.getMessage(messageId);
        Message message = messageService.getMessage(messageId);
        message.addLink(getUriForSelf(uriInfo, message), "self");
        message.addLink(getUriForProfile(uriInfo, message), "profile");
        message.addLink(getUriForComment(uriInfo, message), "comment");
        return message;
    }


    // redirect all the uri with this structure
    @Path("/{messageId}/comments")
    public CommentResource getCommentResource() {
        return new CommentResource();
    }


    private String getUriForSelf(UriInfo uriInfo, Message message) {
        String uri = uriInfo.getAbsolutePathBuilder()
                .path(MessageResource.class) //localhost:8080/messenger/webapi/
                .path(Long.toString(message.getId())) // messages
                .build() // /{messageId}
                .toString();
        return uri;
    }

    private String getUriForProfile(UriInfo uriInfo, Message message) {
        URI uri = uriInfo.getAbsolutePathBuilder()
                .path(ProfileResource.class) //localhost:8080/messenger/webapi/
                .path(message.getAuthor()) // profiles
                .build(); // /{authorName}
        return uri.toString();
    }
    private String getUriForComment(UriInfo uriInfo, Message message) {
        URI uri = uriInfo.getAbsolutePathBuilder()
                .path(MessageResource.class)
                .path(MessageResource.class, "getCommentResource")
                .path(CommentResource.class)
                .resolveTemplate("messageId",message.getId())
                .build();
        return uri.toString();
    }

}
