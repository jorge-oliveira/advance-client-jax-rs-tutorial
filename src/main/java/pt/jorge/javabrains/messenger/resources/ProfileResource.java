package pt.jorge.javabrains.messenger.resources;


import pt.jorge.javabrains.messenger.model.Profile;
import pt.jorge.javabrains.messenger.service.ProfileService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/profiles")
@Consumes(MediaType.APPLICATION_JSON) // accepts the context as a json
@Produces(MediaType.APPLICATION_JSON) // convert the response to jason
public class ProfileResource {
    // create the instance of profile services
    ProfileService profileService = new ProfileService();


//    @GET
//    public String test(){
//        return "test";
//    }

    @GET
    public List<Profile> getAllProfiles(){
        System.out.println("inside method");
        return profileService.getAllProfiles();
    }

/*
    @POST
    public Profile addProfile(Profile profile) {
        return profileService.addProfile(profile);
    }

    @GET
    @Path("/{profileName}")
    public Profile getProfile(@PathParam("profileName") String profileName){
        return profileService.getProfile(profileName);
    }

    @PUT
    @Path("/{profileName}")
    public Profile updateProfile(@PathParam("profileName") String profileName, Profile profile){
        profile.setProfileName(profileName);
        return profileService.updateProfile(profile);
    }

    @DELETE
    @Path("/{profileName}")
    public void deleteProfile(@PathParam("profileName") String profileName){
        profileService.removeProfile(profileName);

    }
*/

}
