package pt.jorge.javabrains.messenger.resources;


import pt.jorge.javabrains.messenger.model.Comment;
import pt.jorge.javabrains.messenger.model.Profile;
import pt.jorge.javabrains.messenger.service.CommentService;
import pt.jorge.javabrains.messenger.service.ProfileService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/") // the @Path annotation is optional for sub resources
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommentResource {

    private CommentService commentService = new CommentService();


    /*
    @GET
    public String test(){
        return "new sub resource";
    }

    @GET
    @Path("/{commentId}") // http://localhost:8088/webapi/messages/1/comments/33
    public String test2(@PathParam("commentId") long commentId){
        return "Method to return the comment id: " + commentId;
    }

    // access to a paramenter from parent
    @GET
    @Path("/parent")
    public String getParentMessageId(@PathParam("messageId") long messageId){
        return "The messageId from the parent is: " + messageId;
    }
    */


    @GET
    @Path("/commentId")
    public List<Comment> getAllComments(@PathParam("messageId") Long messageId){
        return commentService.getAllComments(messageId);
    }

    @POST
    public Comment addMessage(@PathParam("messageId") long messageId, Comment comment){
        return commentService.addComment(messageId, comment);
    }

    @PUT
    @Path("/{commentId}")
    public Comment updateMessage(@PathParam("messageId") long messageId, Comment comment){
        return commentService.updateComment(messageId, comment);
    }

    @DELETE
    @Path("/{commentId}")
    public void deleteComment(@PathParam("messageId") long messageId, @PathParam("commentId") long commentId){
        commentService.removeComment(messageId, commentId);
    }

    @GET
    @Path("/{commentId}")
    public Comment getMessage(@PathParam("messageId") long messageId, @PathParam("commentId") long commentId){
        return commentService.getComment(messageId, commentId);
    }

}
