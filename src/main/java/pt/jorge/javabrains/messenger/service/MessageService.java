package pt.jorge.javabrains.messenger.service;

import org.eclipse.persistence.exceptions.DatabaseException;
import pt.jorge.javabrains.messenger.database.DatabaseClass;
import pt.jorge.javabrains.messenger.exception.DataNotFoundException;
import pt.jorge.javabrains.messenger.model.Message;
import pt.jorge.javabrains.messenger.model.Profile;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class MessageService {

    private Map<Long, Message> messages = DatabaseClass.getMessages();

     public MessageService(){
         messages.put(1L, new Message(1, "Hello World", "bla bla"));
         messages.put(2L, new Message(2, "Hello Porto", "Jorge"));
     }

    public List<Message> getAllMessages(){
        return new ArrayList<Message>(messages.values());
    }

    public List<Message> getAllMessagesForYear(int year){
        List<Message> messagesForYear = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        for (Message message : messages.values()){
            cal.setTime(message.getCreated());
            if(cal.get(Calendar.YEAR) == year){
                messagesForYear.add(message);
            }
        }
        return messagesForYear;
    }

    public List<Message> getAllMessagesPaginated(int start, int size){
        ArrayList<Message> list = new ArrayList<Message>(messages.values());
        if (start + size > list.size()) return new ArrayList<Message>();
        return list.subList(start, start+ size);
    }


   public Message getMessage(Long id) throws DatabaseException {
        Message message = messages.get(id);
        if(message == null){
            throw new DataNotFoundException("Message with " + id + " not found");
        }
        return message;

   }

   public Message addMessage(Message message){
       message.setId(messages.size() + 1 );
       messages.put(message.getId(), message);
       return message;
   }

   public Message updateMessage(Message message){
        if (message.getId() <= 0 ){
            return null;
        }
        messages.put(message.getId(), message);
        return message;
   }

   public Message removeMessage(Long id){
        return messages.remove(id);
   }
}
